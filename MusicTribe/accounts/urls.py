from django.urls import path
from . import views
from app.views import index

app_name = 'accounts'
urlpatterns =[
    path('', index, name='index'),
    path('signup/', views.signup_view, name="signup"),
    path('EDIT_USER/<member_id>',views.EditUserView,name='edit-user'),
    path('EDIT_USER/password',views.UpdatePasswordView,name='update-password'),
    path('ADD_ADMIN/<member_id>',views.createAdmin,name='create-admin'),
    path('ADD_SUPER_ADMIN/<member_id>',views.createSuperAdmin,name='create-super-admin'),
    ]
