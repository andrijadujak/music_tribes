from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.views import generic
from .forms import UserForm,PasswordForm,SignUpForm
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse,reverse_lazy
from app.models import Member
from django.shortcuts import render, get_object_or_404
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth import login, authenticate
from django.shortcuts import redirect
from django.contrib.auth.models import Group

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name= 'registration/signup.html'

def signup_view(request):
    form = UserCreationForm(request.POST)
    if form.is_valid():
        user = form.save()
        user.refresh_from_db()
        user.save()
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')

        user = authenticate(username=username, password=password)
        login(request, user)
        return HttpResponseRedirect(reverse('app:index'))
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})


def EditUserView(request,member_id):
    instance = get_object_or_404(Member,pk=member_id)
    if request.user.member == instance or request.user.is_staff:
        if request.method =='POST':
            form = UserForm(request.POST or None,request.FILES,instance= instance)
            if form.is_valid():
                saved_user=form.save()
                return HttpResponseRedirect(reverse('app:index'))
        else:
            form=UserForm(request.POST or None,instance= instance)
        context={'form':form, 'member':instance}
        return render(request,'registration/edit.html',context)
    else: 
        HttpResponseRedirect(reverse('app:index'))

def createAdmin(request,member_id):
    if request.user.is_staff:
        admin_group= Group.objects.get(name='admin')
        member = get_object_or_404(Member,pk=member_id)
        user = User.objects.get(member = member)
        admin_group.user_set.add(user)
        user.is_staff = True
        user.save()
    return HttpResponseRedirect(reverse('app:index'))

def createSuperAdmin(request,member_id):
    if request.user.is_superadmin:
        member = get_object_or_404(Member,pk=member_id)
        user = User.objects.get(member = member)
        user.is_staff=True
        user.is_superuser = True
        user.save()
    return HttpResponseRedirect(reverse('app:index'))
    
def UpdatePasswordView(request):
    instance = get_object_or_404(User,pk=request.user.id)
    if request.method =='POST':
        form = PasswordForm(request.POST or None,instance= instance)
        if form.is_valid():
            saved_user=form.save()
            return HttpResponseRedirect(reverse('app:index'))
    else:
        form=PasswordForm(request.POST or None,instance= instance)
    context={'form':form}
    return render(request,'registration/updatepassword.html',context)