
from django import forms
from django.contrib.auth.models import User
from django.urls import path
from app.models import Member
from django.contrib.auth.forms import UserCreationForm

class UserForm(forms.ModelForm):
    class Meta:
        model = Member
        fields=['username','email','profile_pic']

        profile_pic=forms.ImageField()

class PasswordForm(forms.ModelForm):
    class Meta:
        model = User
        fields=['password']
        
class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=200)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')