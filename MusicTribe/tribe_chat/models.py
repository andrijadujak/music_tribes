from django.db import models
from django.conf import settings
from app.models import Tribe

class TribeChatRoom(models.Model):

	# Room title
	title 				= models.CharField(max_length=255, unique=True, blank=False,)

	# all users who are authenticated and viewing the chat
	users 				= models.ManyToManyField(settings.AUTH_USER_MODEL, help_text="users who are connected to chat room.", blank=True)

	tribe				=models.OneToOneField(Tribe,on_delete=models.CASCADE)

	def __str__(self):
		return self.title


	def connect_user(self, user):
		is_user_added = False
		if not user in self.users.all():
			self.users.add(user)
			self.save()
			is_user_added = True
		elif user in self.users.all():
			is_user_added = True
		return is_user_added 


	def disconnect_user(self, user):
		is_user_removed = False
		if user in self.users.all():
			self.users.remove(user)
			self.save()
			is_user_removed = True
		return is_user_removed 


	@property
	def group_name(self):
		return "TribeChatRoom-%s" % self.id


class TribeRoomChatMessageManager(models.Manager):
    def by_room(self, room):
        qs = TribeRoomChatMessage.objects.filter(room=room).order_by("-timestamp")
        return qs

class TribeRoomChatMessage(models.Model):
    """
    Chat message created by a user inside a TribeChatRoom
    """
    user                = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    room                = models.ForeignKey(TribeChatRoom, on_delete=models.CASCADE)
    timestamp           = models.DateTimeField(auto_now_add=True)
    content             = models.TextField(unique=False, blank=False,)

    objects = TribeRoomChatMessageManager()

    def __str__(self):
        return self.content

