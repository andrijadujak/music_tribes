from django.apps import AppConfig


class TribeChatConfig(AppConfig):
    name = 'tribe_chat'
