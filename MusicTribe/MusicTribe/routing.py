from channels.auth import AuthMiddlewareStack

from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from django.urls import path, re_path
from tribe_chat import consumers


application = ProtocolTypeRouter({
	'websocket': AllowedHostsOriginValidator(
		AuthMiddlewareStack(
			URLRouter([
					path('homepage/tribe/<int:tribe_id>/', consumers.TribeChatConsumer.as_asgi()),
			])
		)
	),
})