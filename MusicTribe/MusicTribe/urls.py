from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('homepage/', include('app.urls')),
    path('admin/', admin.site.urls),
    path('accounts/',include('accounts.urls')),
    path('USER/',include('django.contrib.auth.urls')),
]
