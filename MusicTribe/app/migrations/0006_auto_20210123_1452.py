# Generated by Django 3.1.3 on 2021-01-23 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_chatbox_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tribe',
            name='name',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
