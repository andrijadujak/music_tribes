# Generated by Django 3.1.3 on 2021-01-22 18:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_chatboxcomment_text'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tribe',
            name='tribe_members',
        ),
        migrations.AddField(
            model_name='member',
            name='tribe',
            field=models.ManyToManyField(to='app.Tribe'),
        ),
    ]
