from .models import Tribe,Playlist,Comment,Song,Member,Comment
from django import forms
from django.contrib.auth.models import User
from django.urls import path

class TribeForm(forms.ModelForm):

    class Meta:
        model= Tribe
        exclude = ['chieftain']
        fields=['name']



class CustomMMCF(forms.ModelMultipleChoiceField):

      def label_from_instance(self, member):
        return "%s" % member.user.username

class UpdateTribeForm(forms.ModelForm):


    class Meta:
        model= Tribe
        exclude =['chieftain']
        fields=['name','tribe_members']

    name = forms.CharField()
    tribe_members = CustomMMCF(
        queryset = Member.objects.all(),
        widget=forms.CheckboxSelectMultiple
        )

class UpdatePlaylistForm(forms.ModelForm):
    class Meta:
        model= Playlist
        fields=['name','description']

    name = forms.CharField()
    description = forms.CharField(widget=forms.Textarea)

class UpdateSongForm(forms.ModelForm):
    class Meta:
        model= Song
        fields=['track_name','track_artist','url','duration']

    track_name = forms.CharField()
    track_artist = forms.CharField()
    url= forms.URLField()
    duration=forms.DurationField()

    def clean(self):
        for field, value in self.cleaned_data.items():
            self.cleaned_data['field']=value.replace('https://www.youtube.com/watch?v=','https://www.youtube.com/embed/')
            self.cleaned_data['field']=value.replace('https://youtu.be/','https://www.youtube.com/embed/')

class PlaylistForm(forms.ModelForm):
    class Meta:
        model= Playlist
        fields=['name','description']

class SongForm(forms.ModelForm):
    class Meta:
        model= Song
        fields=['track_name','track_artist','url','duration']

    def clean(self):
        self.cleaned_data['url'].replace('https://www.youtube.com/watch?v=','https://www.youtube.com/embed/')
        self.cleaned_data['url'].replace('https://youtu.be/','https://www.youtube.com/embed/')

class CommentForm(forms.ModelForm):
    class Meta:
        model= Comment
        fields=['text']