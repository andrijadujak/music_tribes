from django.contrib import admin

from .models import Tribe,Playlist,Song,Chatbox,ChatboxComment,Member,Vote,Comment
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin


class UserAdmin(BaseUserAdmin):
        list_display = ('username', 'email', 'is_staff')
        list_filter = ('is_staff',)
        #this displays after initial user creation for additional information
        fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),
        ('Personal info', {'fields': ('first_name',)}),
        ('Permissions', {'fields': ('is_staff', 'is_active', 'groups')}),
        )
        # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
        # overrides get_fieldsets to use this attribute when creating a user.
        add_fieldsets = (
        (None, {
        'classes': ('wide',),
        'fields': ('username', 'email', 'password1', 'password2')}
        ),
        )
        search_fields = ('username',)
        ordering = ('username',)

        
admin.site.unregister(User)
admin.site.register(User, UserAdmin)


class MemberAdmin(admin.ModelAdmin):
    readonly_fields = ('profile_pic',)

    def profile_pic(self, obj):
        return '<img src="{url}" width="{width}" height={height} />'.format(
            url = obj.headshot.url,
            width=obj.headshot.width,
            height=obj.headshot.height,
            )

admin.site.register(Member)


admin.site.register(Vote)
class MembersInline(admin.TabularInline):
    model= Tribe.tribe_members.through


class TribeAdmin(admin.ModelAdmin):
    fieldsets=[
        ('Tribe name',     {'fields':['name']}),
        ('Chieftain',    {'fields':['chieftain'] } ),
    ]
    inlines=[MembersInline]
    list_display = ('name','chieftain','created_at')
    list_filter = ['created_at']
    search_fields=['name','chieftain__username','tribe_members__user__username']

admin.site.register(Tribe,TribeAdmin)

class CommentsInline(admin.TabularInline):
    model= Song.comments.through

class CommentAdmin(admin.ModelAdmin):
    fieldsets=[
        ('User',    {'fields':['member']}),
        ('Comment text',     {'fields':['text']}),
    ]
    search_fields=['member','text']
    list_filter = ['created_at']

admin.site.register(Comment,CommentAdmin);

class PlaylistAdmin(admin.ModelAdmin):
    fieldsets=[
        ('Playlist name',    {'fields':['name']}),
        ('Tribe',    {'fields':['tribe']}),
        ('Description',     {'fields':['description']}),
        ]
    list_display = ('name','tribe','created_at')
    list_filter = ['created_at']
    search_fields=['name','tribe__name']

admin.site.register(Playlist,PlaylistAdmin)

class SongAdmin(admin.ModelAdmin):
    fieldsets=[
    ('Song information',    {'fields':['track_name']}),
    (None,    {'fields':['track_artist']}),
    (None,    {'fields':['url']}),
    (None,    {'fields':['duration']}),
    ('Playlist',    {'fields':['playlist']}),
    ('Poster',    {'fields':['member']}),
    ]
    inlines=[CommentsInline]
    list_display = ('track_name','track_artist','playlist','member','created_at')
    list_filter = ['created_at']
    search_fields=['track_name','track__artist','playlist','member__user__username']

admin.site.register(Song,SongAdmin)

class ChatboxAdmin(admin.ModelAdmin):
    fieldsets=[
        ('Tribe',    {'fields':['tribe']}),
    ]
    list_display=('name','created_at')
    list_filter=['created_at']
    search_fields=['name']

admin.site.register(Chatbox,ChatboxAdmin)

class ChatboxCommentAdmin(admin.ModelAdmin):
    fieldsets=[
        ('Chatbox',    {'fields':['chatbox']}),
        ('Comment',    {'fields':['member']}),
        (None,    {'fields':['text']}),
    ]
    list_display=('chatbox','member','created_at')
    list_filter=['created_at']
    search_fields=['chatbox','member__author']

admin.site.register(ChatboxComment,ChatboxCommentAdmin)

admin.site.site_url = "/homepage"