from django.urls import path
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .views import create_tribe,create_playlist,create_song,update_song,update_playlist,update_tribe,create_comment,update_comment
from django.conf import settings
from django.conf.urls.static import static

app_name = 'app'
urlpatterns = [
    path('', views.index, name='index'),
    path('tribe/<int:tribe_id>',views.tribeview,name='tribe-view'),
    path('tribe/<int:tribe_id>/playlist/<int:playlist_id>',views.playlistview,name='playlist-view'),
    path('new_tribe', login_required(create_tribe.as_view()), name="new-tribe"),
    path('update_tribe/<pk>', update_tribe.as_view(), name="update-tribe"),
    path('delete_tribe/<tribe_id>', views.delete_tribe, name="delete-tribe"),
    path('<int:tribe_id>/new_playlist/', login_required(create_playlist.as_view()), name="create-playlist"),
    path('update_playlist/<int:tribe_id>/<pk>', update_playlist.as_view(), name="update-playlist"),
    path('delete_playlist/<int:playlist_id>/', views.delete_playlist, name="delete-playlist"),
    path('<int:tribe_id>/<int:playlist_id>/new_song', login_required(create_song.as_view()), name="create-song"),
    path('update_song/<int:playlist_id><pk>', update_song.as_view(), name="update-song"),
    path('delete_song/<song_id>',views.delete_song, name="delete-song"),
    path('<int:song_id>/upvote', views.upvote, name='upvote'),
    path('tribe/<int:tribe_id>/join',views.join_tribe,name='join-tribe'),
    path('commentview', views.commentview,name='commentview'),
    path('commenterview', views.commenterview,name='commenterview'),
    path('update_comment/<int:song_id>/<pk>', update_comment.as_view(), name='update-comment'),
    path('create_comment/<int:song_id>', login_required(create_comment.as_view()), name='create-comment'),
    path('delete_comment/<int:song_id>/<int:comment_id>', views.delete_comment, name='delete-comment'),
    path('search_users',views.search_users,name='search-users'),
]

urlpatterns += static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)
