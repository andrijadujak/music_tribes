from django.shortcuts import render, get_object_or_404
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.core import serializers
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse,reverse_lazy
from .models import Tribe,Playlist,Song,Member,Vote,Comment
from django.views.generic import UpdateView,TemplateView,CreateView
from .forms import TribeForm,UpdateTribeForm,UpdatePlaylistForm,UpdateSongForm,PlaylistForm,SongForm,CommentForm
from tribe_chat.models import TribeChatRoom

from django.conf import settings

DEBUG = False


def index(request):
    tribe_list = Tribe.objects.order_by('created_at')
    context = {'tribe_list': tribe_list }
    return render(request, 'app/homepage.html', context)

def tribeview(request, tribe_id):
    tribe = get_object_or_404(Tribe,pk=tribe_id)
    chatroom = get_object_or_404(TribeChatRoom,tribe=tribe_id)
    playlist = tribe.playlist_set.all()
    context = {  'tribe': tribe,
                 'playlists':playlist,
    }
    context['debug'] = DEBUG
    context['debug_mode'] = settings.DEBUG
    context['room_id'] = chatroom.id
    return render(request, 'app/tribe.html', context)

def playlistview(request,tribe_id,playlist_id):
    tribe= get_object_or_404(Tribe,pk=tribe_id)
    playlist = get_object_or_404(Playlist,pk=playlist_id)
    songs = playlist.song_set.all()
    if request.user.is_authenticated:
        chatroom = get_object_or_404(TribeChatRoom,tribe=tribe_id)
        member = get_object_or_404(Member,pk=request.user.member.id)
        vote = [song.vote_by(member) for song in songs]
        comments = [song.comments.all() for song in songs]
    else:
        vote=[song.anonymous_user for song in songs]
        comments = [song.comments.all() for song in songs]
    context = { 'tribe':tribe,
                'playlist':playlist,
                'songs':zip(songs,vote,comments)
    }
    if request.user.is_authenticated:
        context['debug'] = DEBUG
        context['debug_mode'] = settings.DEBUG
        context['room_id'] = chatroom.id
    return render(request,'app/playlist.html',context)


class create_tribe(CreateView):
    model = Tribe
    form_class = TribeForm
    template_name = 'app/create_tribe.html'

    def form_valid(self, form):
        tribe = form.save(commit=False)
        tribe.chieftain = self.request.user
        tribe.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('app:tribe-view', args={self.object.id})

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['action'] = {'action':'New tribe', 'value':'create'}
        return context

def delete_tribe(request,tribe_id):
    query=Tribe.objects.get(pk=tribe_id)
    query.delete()
    return HttpResponseRedirect(reverse('app:index'))

class update_tribe(UpdateView):
    model = Tribe
    form_class = UpdateTribeForm
    template_name = 'app/create_tribe.html'

    def form_valid(self, form):
        tribe = form.save(commit=False)
        tribe.chieftain = self.request.user
        tribe.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('app:tribe-view', args=(self.object.id))

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['action'] = {'action':'Update tribe', 'value':'Update'}
        return context


class create_playlist(CreateView):
    model = Playlist
    form_class = PlaylistForm
    template_name = 'app/create_playlist.html'

    def dispatch(self, request, *args, **kwargs):
        self.tribe= get_object_or_404(Tribe,pk=kwargs['tribe_id'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.tribe = self.tribe
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('app:playlist-view', args=(self.tribe.id,self.object.id))

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['action'] = {'name':'New playlist', 'value':'create'}
        return context

class update_playlist(UpdateView):
    model = Playlist
    form_class = UpdatePlaylistForm
    template_name = 'app/create_playlist.html'

    def dispatch(self, request, *args, **kwargs):
        self.tribe= get_object_or_404(Tribe,pk=kwargs['tribe_id'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.tribe = self.tribe
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('app:playlist-view', args=(self.tribe.id,self.object.id))

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['action'] = {'name':'Update playlist', 'value':'Update'}
        return context
     
def delete_playlist(request,playlist_id):
    playlist=get_object_or_404(Playlist,pk=playlist_id)
    tribe=get_object_or_404(Tribe,pk=playlist.tribe.id)
    query= Playlist.objects.get(pk=playlist_id)
    query.delete()
    return HttpResponseRedirect(reverse('app:playlist-view',args=(tribe.id,playlist_id)))

class create_song(CreateView):
    model = Song
    form_class = SongForm
    template_name = 'app/create_song.html'

    def dispatch(self, request, *args, **kwargs):
        self.playlist= get_object_or_404(Playlist,pk=kwargs['playlist_id'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.playlist = self.playlist
        form.instance.member = self.request.user.member
        form.save()
        return super().form_valid(form)

    def get_success_url(self,*args,**kwargs):
        return reverse('app:playlist-view', args=(self.playlist.tribe.id, self.playlist.id))

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['action'] = {'action':'New song', 'value':'create'}
        return context

class update_song(UpdateView):
    model = Song
    form_class = UpdateSongForm
    template_name = 'app/create_song.html'

    

    def dispatch(self, request, *args, **kwargs):
        self.playlist= get_object_or_404(Playlist,pk=kwargs['playlist_id'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.playlist = self.playlist
        form.save()
        return super().form_valid(form)

    def get_success_url(self,*args,**kwargs):
        return reverse('app:playlist-view', args=(self.playlist.tribe.id, self.playlist.id))

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['action'] = {'action':'Update song', 'value':'update'}
        return context

def delete_song(request,song_id):
    song=get_object_or_404(Song,pk=song_id)
    playlist=get_object_or_404(Playlist,pk=song.playlist.id)
    tribe=get_object_or_404(Tribe,pk=playlist.tribe.id)
    query= Song.objects.get(pk=song_id)
    query.delete()
    return HttpResponseRedirect(reverse('app:playlist-view',args=(tribe.id,playlist.id)))

def vote(request,song_id, upvote):
    song = get_object_or_404(Song, pk=song_id)
    vote = Vote.objects.filter(member=request.user.member,song=song).first()
    if vote:
        if vote.upvote == upvote:
            vote.delete()
            return None
        else:
            vote.upvote = upvote
    else:
        vote=Vote(song=song, upvote=upvote, member=request.user.member)
    try:
        vote.full_clean()
        vote.save()
    except:
        return None
    else:
        return vote


def upvote(request, song_id):
    song=get_object_or_404(Song,pk=song_id)
    playlist=get_object_or_404(Playlist,pk=song.playlist.id)
    tribe=get_object_or_404(Tribe,pk=playlist.tribe.id)
    if request.method =='POST' and request.user.is_authenticated:
        tribe_members=tribe.tribe_members.all()
        member=get_object_or_404(Member,pk=request.user.member.id)
        if member in tribe_members:
            vote(request,song_id, True)

    return HttpResponseRedirect(reverse('app:playlist-view',args=(tribe.id,playlist.id)))

def join_tribe(request, tribe_id):
    tribe = get_object_or_404(Tribe,pk=tribe_id)
    member = get_object_or_404(Member,pk=request.user.member.id)
    tribe.tribe_members.add(member)
    return HttpResponseRedirect(reverse('app:tribe-view',args=[tribe.id]))

def commentview(request):
    if request.is_ajax and request.method =="GET":
        song_id  = request.GET.get("song_id",None)
        song = get_object_or_404(Song,pk=song_id)
        comments = song.comments.all()
        ser_comments = serializers.serialize('json', comments)
        print(ser_comments)
        return JsonResponse({"instance": ser_comments}, status=200)

def commenterview(request):
    if request.is_ajax and request.method=="GET":
        member_id = request.GET.get("member_id",None)
        member= get_object_or_404(Member,pk=member_id)
        print(member.username)
        return JsonResponse({"name":member.username}, status=200)


class create_comment(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'app/create_comment.html'

    def dispatch(self, request, *args, **kwargs):
        self.member = get_object_or_404(Member,pk=request.user.member.id)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        if self.request.user.user.is_authenticated:
            song= get_object_or_404(Song,pk=self.kwargs['song_id'])
            form.instance.member = self.request.user.member
            form.save()
            song.comments.add(form.instance.id)
            return super().form_valid(form)
        else:
            return HttpResponseRedirect(reverse('app:playlist-view',args=(tribe.id,playlist.id)))

    def get_success_url(self):
        song= get_object_or_404(Song,pk=self.kwargs['song_id'])
        return reverse('app:playlist-view', args=(song.playlist.tribe.id,song.playlist.id))

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['action'] = {'name':'New comment', 'value':'create'}
        return context

class update_comment(UpdateView):
    model = Comment
    form_class = CommentForm
    template_name = 'app/create_comment.html'

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        return super().form_valid(form)

    def get_success_url(self):
        song= get_object_or_404(Song,pk=self.kwargs['song_id'])
        return reverse('app:playlist-view', args=(song.playlist.tribe.id,song.playlist.id))

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        context['action'] = {'name':'Update comment', 'value':'Update'}
        return context

def delete_comment(request,song_id,comment_id):
    comment=get_object_or_404(Comment,pk=comment_id)
    song=get_object_or_404(Song,pk=song_id)
    playlist=get_object_or_404(Playlist,pk=song.playlist.id)
    tribe=get_object_or_404(Tribe,pk=playlist.tribe.id)
    query= comment.objects.get(pk=song_id)
    query.delete()
    return HttpResponseRedirect(reverse('app:playlist-view',args=(tribe.id,playlist.id)))

def search_users(request):
    if request.method =="POST":
        user = request.POST['user']
        user_list = Member.objects.filter(username__contains= user)
        return render(request,'app/user_search.html',{'user_list':user_list, })
    else:
        return render(request,'app/user_search.html')