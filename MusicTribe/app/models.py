from django.db import models
from django.contrib.auth.models import User
import datetime
from django.utils import timezone
from django.conf import settings
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.core.exceptions import PermissionDenied
from django.db.models.signals import post_save


@receiver(pre_delete, sender=User)
def delete_user(sender, instance, **kwargs):
    if instance.is_superuser:
        raise PermissionDenied

class TimeStamped(models.Model):
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = timezone.now()

        self.updated_at = timezone.now()
        return super(TimeStamped, self).save(*args, **kwargs)

    class Meta:
        abstract = True

class Member(TimeStamped):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    username=models.CharField(max_length=200,unique=True,default="")
    email=models.CharField(max_length=200,default="")
    password=models.CharField(max_length=200,default="")
    profile_pic = models.ImageField(default='default.png')

    def __str__(self):
        return self.username[:80]

@receiver(post_save, sender=User)
def update_member_signal(sender, instance, created, **kwargs):
    if created:
        Member.objects.create(user=instance,username=instance.username,email=instance.email,password=instance.password)
    instance.member.save()

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
		instance.member.save()



class Tribe(TimeStamped):
    name = models.CharField(max_length=200,unique=True)
    chieftain = models.ForeignKey(User,on_delete=models.CASCADE)
    tribe_members = models.ManyToManyField(Member)

    def __str__(self):
        return self.name[:80]

    def is_chieftain(self,request):
        if (self.chieftain == request.user):
            return True
        else:
            return False



class Playlist(TimeStamped):
    tribe = models.ForeignKey(Tribe,on_delete=models.CASCADE)
    name = models.CharField(max_length=200,unique=True)
    description = models.TextField()

    def __str__(self):
        return self.name[:80]

class Comment(TimeStamped):
    text = models.TextField(blank=False)
    member = models.ForeignKey(Member, on_delete=models.CASCADE)


    def __str__(self):
        return self.text[:80]

    def author(self):
        return self.member.user.username   


class Song(TimeStamped):
    playlist = models.ForeignKey(Playlist,on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    track_name = models.CharField(max_length=200)
    track_artist = models.CharField(max_length=200)
    url = models.CharField(max_length=512)
    duration = models.DurationField()
    upvotes = models.IntegerField(default=0)
    comments = models.ManyToManyField(Comment)

    def __str__(self):
        return self.track_name[:80]

    def vote_score(self):
        upvotes= self.vote_set.filter(upvote=True).count()
        return upvotes
    
    def vote_by(self,member):
        return Vote.objects.filter(member=member,song=self).first()

    def get_url(self):
        url=self.url
        url=url.replace('https://www.youtube.com/watch?v=','https://www.youtube.com/embed/')
        url=url.replace('https://youtu.be/','https://www.youtube.com/embed/')
        return url

    def anonymous_user(self):
        return False





class Vote(TimeStamped):
    song = models.ForeignKey(Song,on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE,default=None)
    upvote = models.BooleanField(null=False,default=True)

    class Meta:
        constraints =[
            models.UniqueConstraint(name='user_vote',fields=['song','member'])
            ]
    def __str__(self):
        return f"{self.member.username} voted on {self.song.track_name}"

class Chatbox(TimeStamped):
    tribe = models.ForeignKey(Tribe,on_delete=models.CASCADE)
    name =models.CharField(max_length=200,default='')

    def __str__(self):
        return self.name[:80]

class ChatboxComment(TimeStamped):
    chatbox = models.ForeignKey(Chatbox,on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    text = models.TextField(default='')

    def __str__(self):
        return self.text[:80]

    def author(self):
        return self.member.user.username
